//
//  PDFVC.swift
//  Glucerna
//
//  Created by Long Quách Phi on 3/23/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit
import WebKit

class PDFViewController: UIViewController{
    
    @IBOutlet weak var webView: UIWebView!
    public var fileName : String?
    
    @IBAction func onClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func repareLoadPDF(){
        if let pdfURL = Bundle.main.url(forResource: fileName!, withExtension: "pdf", subdirectory: nil, localization: nil)  {
            do {
                let data = try Data(contentsOf: pdfURL)
                webView.load(data, mimeType: "application/pdf", textEncodingName:"", baseURL: pdfURL.deletingLastPathComponent())
            }
            catch {
                print("Data file pdf not invalid")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        repareLoadPDF()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
