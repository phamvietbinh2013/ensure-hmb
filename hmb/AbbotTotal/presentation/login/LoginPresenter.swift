//
//  LoginPresenter.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import Foundation
import UIKit

class LoginPresenter {
    fileprivate let loginService : Login
    weak fileprivate var loginView : LoginView?
    
    init(loginService : Login){
        self.loginService = loginService
    }
    
    func attachView(_ view : LoginView){
        loginView = view
    }
    
    func detachView() {
        loginView = nil
    }
    
    func doLogin(id : String, password : String){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        loginView?.showLoading("Processing ...")
        loginService.login(id: id, password: password) { success, response in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.loginView?.hideLoading()
            if success{
                if let status = response.status {
                    if (status.elementsEqual("true")){
                        self.loginView?.loginSuccess(response)
                    } else {
                        self.loginView?.loginFailed(response.msg!)
                    }
                }
                else{
                    self.loginView?.loginFailed("Kết nối thất bại")
                }
            }
            else{
                self.loginView?.loginFailed("Kết nối thất bại")
            }
        }
    }
}
