//
//  LoginViewController.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 5/31/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import PopupDialog
import Async
import SwiftSpinner

class LoginViewController: BaseContentVC {
    
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var imgbg: UIImageView!
    @IBOutlet weak var txtFieldID: UITextField!
    @IBOutlet weak var txtFieldPass: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var viewUserName: UIView!
    @IBOutlet weak var viewPassword: UIView!
    var userid = ""
    
    fileprivate let presenter = LoginPresenter(loginService: Login())
    
    @IBAction func onChangeServerClick(_ sender: UIButton) {
        let viewController = LoginMenuVC(nibName: "LoginMenuVC", bundle: nil)
        viewController.preferredContentSize = CGSize(width: 480, height: 24)
        viewController.base = self
        let navController = UINavigationController(rootViewController: viewController)
        navController.modalPresentationStyle = .popover
        if let pctrl = navController.popoverPresentationController {
            pctrl.delegate = self
            pctrl.sourceView = sender
            pctrl.sourceRect = sender.bounds
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    @IBAction func abtnLogin() {
        userid = txtFieldID.text!
        presenter.doLogin(id: txtFieldID.text!, password: txtFieldPass.text!)
    }
    
    func loadImage(){
        loadResourceToImage("bg_page_login", imgbg)
        viewUserName.roundCorderWithBorder(UIColor(hexString: "#26a0df")!, 30)
        viewPassword.roundCorderWithBorder(UIColor(hexString: "#26a0df")!, 30)
    }
    
    func repair(){
        txtFieldPass.placeholder = "Mật Khẩu"
        txtFieldPass.attributedPlaceholder = NSAttributedString(string: txtFieldPass.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
        txtFieldID.placeholder = "ID Nhân Viên"
        txtFieldID.attributedPlaceholder = NSAttributedString(string: txtFieldID.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(self)
        repair()
        loadImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension LoginViewController : LoginView {
    
    func showLoading(_ message: String) {
        SwiftSpinner.show(message)
    }
    
    func hideLoading() {
        SwiftSpinner.hide()
    }
    
    func loginSuccess(_ data: LoginModel) {
        let userdata = UserData(JSONString: "{}")
        userdata?.userid = userid
        userdata?.sysid = data.sysid!
        userdata?.name = data.name!
        userdata?.location = data.location
        userdata?.group = data.group
        Utils.saveToCache(key: CacheKey.userdata, value: (userdata?.toJSONString()!)!)
        let vc = HospitalVC(nibName: "HospitalVC", bundle: nil)
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    func loginFailed(_ meassage : String) {
        let vc = MessagerDialogVC(nibName: "MessagerDialogVC", bundle: nil)
        vc.messenge = meassage
        let containerAppearance = PopupDialogContainerView.appearance()
        containerAppearance.cornerRadius = 15
        let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
        present(popup, animated: true, completion: nil)
        
    }
    
    
}
