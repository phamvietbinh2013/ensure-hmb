//
//  HospitalView.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 1/14/20.
//  Copyright © 2020 bp. All rights reserved.
//

import Foundation
protocol HospitalView : BaseView {
    func onGetHCP(_ data : [HospitalModel])
    func onGetHCPFailed(_ message : String)
    func pushDurationSuccess(_ data: ReturnDurationModel)
    func pushDurationFailed(_ message : String)
}
