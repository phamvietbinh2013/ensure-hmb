//
//  HospitalVC.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 1/13/20.
//  Copyright © 2020 bp. All rights reserved.
//

import UIKit
import SearchTextField
import Async
import PopupDialog
import SearchTextField
import EVReflection
import SwiftSpinner
import CoreLocation

class HospitalVC: BaseContentVC {
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet var imgGoldBars: [UIImageView]!
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var txtSearch: SearchTextField!
    
    @IBAction func onMenuTap(_ sender: UIButton) {
        let viewController = HomeMenuVC(nibName: "HomeMenuVC", bundle: nil)
        viewController.preferredContentSize = CGSize(width: 480, height: 220)
        viewController.base = self
        let navController = UINavigationController(rootViewController: viewController)
        navController.modalPresentationStyle = .popover
        if let pctrl = navController.popoverPresentationController {
            pctrl.delegate = self
            pctrl.sourceView = sender
            pctrl.sourceRect = sender.bounds
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    @IBAction func onHospitalTap(_ sender: Any) {
        txtSearch.becomeFirstResponder()
    }
    
    @IBAction func onNextTap(_ sender: Any) {
        if let _ = selectedHosp {
            let vc = HomeVC(nibName: "HomeVC", bundle: nil)
            presentDetail(vc)
        } else {
            showAlertWith(message: "Chưa chọn bệnh viện")
        }
    }
    
    fileprivate var hospitalList = [HospitalModel]()
    fileprivate var selectedHosp : HospitalModel?
    private let userData : UserData = UserData(JSONString: Utils.getFromCache(key: CacheKey.userdata)!)!
    private let presenter = HospitalPresenter(getHcp: GetHospital(), durationService: PushDuration())
    private var durationsRecords : [DurationRecord] = []
    private var completeSendDuration = 0
    private var totalDurationRecord = 0
    private var locationManager = CLLocationManager()
    private var userLocation = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(self)
        setupUI()
        locationManager.delegate = self
        reparingUserLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        clearData()
        presenter.doGetHCP(id: userData.userid!)
    }
    
    override func loadImages() {
        setupToolbar(imgBackground, imgGoldBars, imgLogo)
    }
    
    func setupUI(){
        viewContent.layer.cornerRadius = 20
        viewContent.clipsToBounds = true
        viewContent.layer.shadowColor = UIColor.black.cgColor
        viewContent.layer.shadowOffset = CGSize(width: 2, height: 5)
        viewContent.layer.shadowRadius = 20
        viewContent.layer.shadowOpacity = 1
        txtSearch.placeholder = "Bệnh viện"
        txtSearch.attributedPlaceholder = NSAttributedString(string: txtSearch.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
        txtSearch.theme.font = UIFont.systemFont(ofSize: 17)
        txtSearch.theme.bgColor = UIColor.white
        txtSearch.theme.separatorColor = UIColor.black
        txtSearch.theme.fontColor = UIColor.black
        txtSearch.theme.cellHeight = 40
        txtSearch.startVisible = true
        txtSearch.highlightAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.3380619884, green: 0.7089733481, blue: 0.9751031995, alpha: 1)]
        txtSearch.itemSelectionHandler = {items, itemPosition in
            self.view.endEditing(true)
            self.txtSearch.startVisibleWithoutInteraction = false
            if let filteredItem = items[itemPosition] as? HostpitalFilterItem, let id = filteredItem.id, id < self.hospitalList.count  {
                self.selectedHosp = self.hospitalList[id]
                self.txtSearch.text = self.selectedHosp?.name
            }
        }
    }
    
    func prepareDurationRequest(_ durationRecord : DurationRecord) -> Dictionary<String, Any>?{
        var durationRequest : Dictionary<String, Any>?
        durationRequest = durationRecord.durations
        durationRequest!["clientid"] = durationRecord.clientid
        durationRequest!["appid"] = durationRecord.appid
        durationRequest!["appver"] = durationRecord.appver
        durationRequest!["usersysid"] = durationRecord.usersysid
        durationRequest!["starttime"] = durationRecord.starttime
        durationRequest!["endtime"] = durationRecord.endtime
        durationRequest!["usersysid"] = durationRecord.usersysid
        durationRequest!["hcpid"] = durationRecord.hcpid
        return durationRequest
    }
    
    func reparingUserLocation(){
        locationManager.startUpdatingLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
    }
    
    func isLocationServiceEnabled() -> Bool{
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted:
                return false
            case .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            @unknown default:
                return false
            }
        }
        return false
    }
    
    func showLocationServiceAlert(){
        let vc = MessagerDialogVC(nibName: "MessagerDialogVC", bundle: nil)
        let containerAppearance = PopupDialogContainerView.appearance()
        containerAppearance.cornerRadius = 15
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted:
                vc.messenge = "Không xác định được vị trí người sử dụng"
                vc.callbackPositive = {vc in
                    self.locationManager.requestAlwaysAuthorization()
                    self.locationManager.requestWhenInUseAuthorization()
                }
                let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                present(popup, animated: true, completion: nil)
            case .denied:
                vc.messenge = "Chưa cấp quyền xác định vị trí, để tiếp tục vui lòng vào Cài đặt → Quyền riêng tư → Dịch vụ định vị → HCPAdult và cấp quyền cho ứng dụng"
                vc.callbackPositive = {vc in
                    self.locationManager.requestAlwaysAuthorization()
                    self.locationManager.requestWhenInUseAuthorization()
                }
                let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                present(popup, animated: true, completion: nil)
            case .authorizedAlways, .authorizedWhenInUse:
                vc.messenge = "Lấy lại vị trí người dùng thành công, ấn đồng ý để tiếp tục"
                let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                present(popup, animated: true, completion: nil)
            @unknown default:
                vc.messenge = "Không xác định được vị trí người sử dụng"
                vc.callbackPositive = {vc in
                    self.locationManager.requestAlwaysAuthorization()
                    self.locationManager.requestWhenInUseAuthorization()
                }
                let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                present(popup, animated: true, completion: nil)
            }
        }
        else{
            vc.messenge = "Chưa bật dịch vụ định vị, để tiếp tục vui lòng vào Cài đặt, Quyền riêng tư và bật dịch vụ định vị"
            vc.callbackPositive = {vc in
                self.locationManager.requestAlwaysAuthorization()
                self.locationManager.requestWhenInUseAuthorization()
            }
            let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
            present(popup, animated: true, completion: nil)
        }
        reparingUserLocation()
    }
    
}

extension HospitalVC : HospitalView {
    
    func pushDurationSuccess(_ data: ReturnDurationModel) {
        Utils.clearDuration(data.clientid!)
        durationsRecords.remove(at: durationsRecords.count-1)
        completeSendDuration = completeSendDuration + 1
        showLoading("\(completeSendDuration)/\(totalDurationRecord) record uploaded ...")
        if durationsRecords.count>0 {
            presenter.pushDuration(durationRequestModel: prepareDurationRequest(durationsRecords.last!)!)
        } else {
            Async.main(after: 1, {
                self.hideLoading()
            })
        }
    }
    
    
    func showLoading(_ message: String) {
        SwiftSpinner.show(message)
    }
    
    func hideLoading() {
        SwiftSpinner.hide()
    }
    
    func isContain(item : SearchTextFieldItem, in items:[SearchTextFieldItem]) -> Bool{
        for obj in items{
            if obj.title == item.title{
                return true
            }
        }
        return false
    }
    
    func onGetHCP(_ data: [HospitalModel]) {
        hospitalList = data
        if data.count > 0{
            var items : [SearchTextFieldItem] = []
            for i in 0...data.count - 1 {
                items.append(HostpitalFilterItem(title: data[i].name!, id: i))
            }
            txtSearch.filterItems(items)
        }
    }
    
    func onGetHCPFailed(_ message: String) {
        showAlertWith(message: message)
    }
    
    func pushDurationFailed(_ message : String){
        hideLoading()
    }
    
    func clearData(){
        txtSearch.text = ""
    }
}

extension HospitalVC: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedAlways || status == CLAuthorizationStatus.authorizedWhenInUse{
            SwiftSpinner.hide()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        userLocation = locValue
    }
    
}
