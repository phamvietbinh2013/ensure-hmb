//
//  HospitalPresenter.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 1/14/20.
//  Copyright © 2020 bp. All rights reserved.
//

import Foundation
import UIKit

class HospitalPresenter {
    fileprivate let getHcp : GetHospital
    fileprivate let durationService : PushDuration
    weak fileprivate var view : HospitalView?
    
    init(getHcp : GetHospital, durationService : PushDuration){
        self.getHcp = getHcp
        self.durationService = durationService
    }
    
    func attachView(_ view : HospitalView){
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func doGetHCP(id : String) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        view?.showLoading("Processing ...")
        getHcp.postHCP(id: id){ success, response in
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.view?.hideLoading()
            if success {
                self.view?.onGetHCP(response.data!)
            } else {
                self.view?.onGetHCPFailed("Kết nối thất bại")
            }
        }
    }
    
    func pushDuration(durationRequestModel: Dictionary<String, Any>){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        durationService.pushDuration(durationRequestModel: durationRequestModel) { success, response in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if success{
                if let status = response.status {
                    if (status.elementsEqual("true")){
                        self.view?.pushDurationSuccess(response)
                    } else {
                        self.view?.pushDurationFailed(response.msg!)
                    }
                }
                else{
                    self.view?.pushDurationFailed("Kết nối thất bại")
                }
            }
            else{
                self.view?.pushDurationFailed("Kết nối thất bại")
            }
        }
    }
}
