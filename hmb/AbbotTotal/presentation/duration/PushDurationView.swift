//
//  ViewController.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 7/3/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

protocol PushDurationView: BaseView {
    func pushDurationSuccess(_ data: ReturnDurationModel)
    func pushDurationFailed(_ message : String)
}
