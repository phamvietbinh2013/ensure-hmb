//
//  DetailView.swift
//  Pediatric
//
//  Created by Le Nhan on 11/15/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import Foundation


protocol DetailView {
    
    func loadImages()
    
    func resetAnimation()
    
    func startAnimation()
    
}
