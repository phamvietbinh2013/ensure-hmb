//
//  CustomerVC.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 1/14/20.
//  Copyright © 2020 bp. All rights reserved.
//

import UIKit

class CustomerVC: BaseContentVC {
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet var imgGoldBars: [UIImageView]!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var segmentMale: UISegmentedControl!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var txtDateInput: DateTextField!
    @IBOutlet weak var imgValidateDate: UIImageView!
    @IBOutlet weak var txtStrength: UITextField!
    @IBOutlet weak var imgValidateStrength: UIImageView!
    @IBAction func onBackTap(_ sender: Any) {
        dismissDetail()
    }
    @IBAction func onMenuTap(_ sender: UIButton) {
        inFlowMenuTap(sender)
    }
    @IBAction func onStartTap(_ sender: UIButton) {
        boucingView(sender, 1.1, 0.3)
        if let date = txtDateInput.date {
            let currentYear = Calendar.current.component(.year, from: Date())
            let inputYear = Calendar.current.component(.year, from: date)
            let age = currentYear - inputYear
            if age>=18 && age<=99 {
                hideAlert(imgValidateDate)
                if let strengthText = txtStrength.text, let strength = Float(strengthText) {
                    if strength>=0.0 && strength<=60.0 {
                        hideAlert(imgValidateStrength)
                        print(age," ", strength)
                        presentDetail(ChartVC(nibName: "ChartVC", bundle: nil))
                    } else {
                        showAlert(imgValidateStrength)
                    }
                } else {
                    showAlert(imgValidateStrength)
                }
            } else {
                showAlert(imgValidateDate)
            }
        } else {
            showAlert(imgValidateDate)
            
        }
    }
    
    func hideAlert(_ imageView : UIImageView){
        UIView.animate(withDuration: 0.25, animations: {
            imageView.layer.opacity = 0
        })
    }
    
    func showAlert(_ imageView : UIImageView){
        self.boucingView(imageView, 1.5, 0.35)
        UIView.animate(withDuration: 0.25, animations: {
            imageView.layer.opacity = 1
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func loadImages() {
        setupToolbar(imgBackground, imgGoldBars, imgLogo)
    }
    
    func setupUI(){
        viewContent.layer.cornerRadius = 20
        viewContent.clipsToBounds = true
        viewContent.layer.shadowColor = UIColor.black.cgColor
        viewContent.layer.shadowOffset = CGSize(width: 2, height: 5)
        viewContent.layer.shadowRadius = 20
        viewContent.layer.shadowOpacity = 1
        segmentMale.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)
        let font = UIFont.systemFont(ofSize: 23, weight: .semibold)
        segmentMale.setTitleTextAttributes([NSAttributedString.Key.font: font], for: UIControl.State.normal)
        
        btnStart.applyCornerGradient(colours: [UIColor(hexString: "#9d7528")!,UIColor(hexString: "#fef25b")!,UIColor(hexString: "#9d7528")!], corner: 10)
        txtDateInput.dateFormat = .dayMonthYear
        txtDateInput.separator = "/"
        txtDateInput.placeholder = "__/__/____"
        txtDateInput.attributedPlaceholder = NSAttributedString(string: txtDateInput.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
    }
    
}
