//
//  Page2VC.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 1/15/20.
//  Copyright © 2020 bp. All rights reserved.
//

import UIKit

class Page2VC: BaseContentVC {
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet var imgGoldBars: [UIImageView]!
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var imgContent2: UIImageView!
    @IBOutlet weak var imgContent: UIImageView!
    @IBAction func onMenuTap(_ sender: UIButton) {
        inFlowMenuTap(sender)
    }
    @IBAction func onNextTap(_ sender: Any) {
        presentDetail(Page3VC(nibName: "Page3VC", bundle: nil))
    }
    
    @IBAction func onBackTap(_ sender: Any) {
        dismissDetail()
    }
    
    fileprivate var originContentX : CGFloat = 0
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        originContentX = imgContent2.layer.position.x
    }
    
    override func loadImages() {
        loadResourceToImage("img_2d_2", imgContent)
        loadResourceToImage("img_2d_2_1", imgContent2)
        setupToolbar(imgBackground, imgGoldBars, imgLogo)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imgContent2.layer.opacity = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onViewTap)))
    }
    
    @objc func onViewTap(){
        self.imgContent2.layer.position.x = originContentX - 100
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.25, initialSpringVelocity: 1, options: [], animations: {
            self.imgContent2.layer.opacity = 1
            self.imgContent2.layer.position.x = self.originContentX
        }, completion: nil)
    }
    
}
