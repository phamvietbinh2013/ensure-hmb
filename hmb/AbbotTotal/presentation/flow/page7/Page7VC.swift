//
//  Page7VC.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 1/16/20.
//  Copyright © 2020 bp. All rights reserved.
//

import UIKit
import Async

class Page7VC: BaseContentVC {
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet var imgGoldBars: [UIImageView]!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgContent: UIImageView!
    @IBOutlet weak var imgContentLeft: UIImageView!
    @IBOutlet weak var imgMedal: UIImageView!
    @IBOutlet weak var imgEnsureMain: UIImageView!
    @IBOutlet weak var imgEnsureEffect: UIImageView!
    @IBAction func onMenuTap(_ sender: UIButton) {
        inFlowMenuTap(sender)
    }
    @IBAction func onNextTap(_ sender: Any) {
        presentDetail(EndingVC(nibName: "EndingVC", bundle: nil))
    }
    
    @IBAction func onBackTap(_ sender: Any) {
        dismissDetail()
    }
    override func loadImages() {
        loadResourceToImage("img_2d_6", imgContent)
        loadResourceToImage("img_2d_6_left", imgContentLeft)
        loadResourceToImage("img_2d_6_logo_slogan", imgMedal)
        loadResourceToImage("img_2d_6_logo", imgEnsureMain)
        loadResourceToImage("img_2d_6_logo", imgEnsureEffect)
        setupToolbar(imgBackground, imgGoldBars, imgLogo)
        Async.main(after: 0.5, {
            self.imgEnsureEffect.tintImageColor(color: UIColor.white)
        }).main(after: 0.1, {
            self.imgEnsureEffect.startShimmer()
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        imgEnsureEffect.stopShimmer()
    }
    
}
