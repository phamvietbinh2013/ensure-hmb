//
//  HomeVC.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 1/14/20.
//  Copyright © 2020 bp. All rights reserved.
//

import UIKit
import Async

class HomeVC: BaseContentVC {
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgLogoMain: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBAction func onNextTap(_ sender: Any) {
        let vc = CustomerVC(nibName: "CustomerVC", bundle: nil)
        presentDetail(vc)
    }
    
    override func loadImages() {
        loadResourceToImage("img_2d_home", imgBackground)
        loadResourceToImage("img_2d_home_logo", imgLogoMain)
        loadResourceToImage("img_2d_home_logo", imgLogo)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Async.main(after: 0.5, {
            self.imgLogo.tintImageColor(color: UIColor.white)
        }).main(after: 0.1, {
            self.imgLogo.startShimmer()
        })
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        imgLogo.stopShimmer()
    }
    
}
