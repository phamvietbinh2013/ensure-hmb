//
//  Page3VC.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 1/15/20.
//  Copyright © 2020 bp. All rights reserved.
//

import UIKit
import Async

class Page3VC: BaseContentVC {
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet var imgGoldBars: [UIImageView]!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet var imgNotes: [UIImageView]!
    @IBOutlet weak var imgContent: UIImageView!
    @IBOutlet weak var imgPeople: UIImageView!
    @IBOutlet weak var imgPeopleGlow: UIImageView!
    @IBOutlet var viewTaps: [UIView]!
    
    @IBAction func onMenuTap(_ sender: UIButton) {
        inFlowMenuTap(sender)
    }
    @IBAction func onNextTap(_ sender: Any) {
        presentDetail(Page4VC(nibName: "Page4VC", bundle: nil))
    }
    
    @IBAction func onBackTap(_ sender: Any) {
        dismissDetail()
    }
    
    override func loadImages() {
        setupToolbarUnisversal(imgBackground, imgGoldBars, imgLogo)
        loadResourceToImage("img_2d_3", imgContent)
        loadResourceToImage("img_2d_3_people", imgPeopleGlow)
        loadResourceToImage("img_2d_3_people", imgPeople)
        Async.main(after: 0.5, {
            self.imgPeopleGlow.startGlowingWithColor(color: UIColor.yellow.withAlphaComponent(0.6), intensity: 0.6)
        })
        for i in 0...viewTaps.count-1 {
            viewTaps[i].tag = i
            viewTaps[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onViewTap(_:))))
            imgNotes[i].layer.opacity = 0
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        imgPeopleGlow.stopGlowing()
    }
    
    @objc func onViewTap(_ sender : UITapGestureRecognizer){
        let index = sender.view?.tag ?? 0
        UIView.animate(withDuration: 0.25, animations: {
            self.imgNotes[index].layer.opacity = 1
        })
    }
}
