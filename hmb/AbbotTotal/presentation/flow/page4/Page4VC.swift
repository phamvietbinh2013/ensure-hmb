//
//  Page4VC.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 1/15/20.
//  Copyright © 2020 bp. All rights reserved.
//

import UIKit
import Async

class Page4VC: BaseContentVC {
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet var imgGoldBars: [UIImageView]!
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var imgContent2: UIImageView!
    @IBOutlet weak var imgContent3: UIImageView!
    @IBOutlet weak var imgContent: UIImageView!
    @IBAction func onMenuTap(_ sender: UIButton) {
        inFlowMenuTap(sender)
    }
    @IBAction func onNextTap(_ sender: Any) {
        presentDetail(Page5VC(nibName: "Page5VC", bundle: nil))
    }
    
    @IBAction func onBackTap(_ sender: Any) {
        dismissDetail()
    }
    
    override func loadImages() {
        loadResourceToImage("img_2d_4", imgContent)
        loadResourceToImage("img_2d_4_effect", imgContent2)
        loadResourceToImage("img_2d_4_effect", imgContent3)
        setupToolbar(imgBackground, imgGoldBars, imgLogo)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Async.main(after: 0.5, {
            self.imgContent3.tintImageColor(color: UIColor.white)
        }).main(after: 0.1, {
            self.imgContent3.startShimmer()
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        imgContent3.stopShimmer()
    }
    
}
