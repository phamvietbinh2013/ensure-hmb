//
//  Page5VC.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 1/15/20.
//  Copyright © 2020 bp. All rights reserved.
//

import UIKit

class Page5VC: BaseContentVC {
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet var imgGoldBars: [UIImageView]!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgContent: UIImageView!
    @IBOutlet weak var imgContent1: UIImageView!
    @IBOutlet weak var imgContentEffectTopNorma: UIImageView!
    @IBOutlet weak var imgContentEffectBottomNorma: UIImageView!
    @IBOutlet weak var imgContentEffectTop: UIImageView!
    @IBOutlet weak var imgContentEffectBottom: UIImageView!
    @IBOutlet weak var viewTapTop: UIView!
    @IBOutlet weak var viewTapBottom: UIView!
    
    @IBAction func onMenuTap(_ sender: UIButton) {
        inFlowMenuTap(sender)
    }
    
    @IBAction func onNextTap(_ sender: Any) {
        presentDetail(Page7VC(nibName: "Page7VC", bundle: nil))
    }
    
    @IBAction func onBackTap(_ sender: Any) {
        dismissDetail()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTapTop.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTopTap)))
        viewTapBottom.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onBottomTap)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imgContentEffectBottom.layer.opacity = 0
        imgContentEffectTop.layer.opacity = 0
    }
    
    @objc func onTopTap(){
        UIView.animate(withDuration: 0.5, delay: 0, options: [.repeat, .autoreverse], animations: {
            UIView.setAnimationRepeatCount(3)
            self.imgContentEffectTop.layer.opacity = 1
        }, completion: { _ in
            self.imgContentEffectTop.layer.opacity = 0
        })
    }
    
    @objc func onBottomTap(){
        UIView.animate(withDuration: 0.5, delay: 0, options: [.repeat, .autoreverse], animations: {
            UIView.setAnimationRepeatCount(3)
            self.imgContentEffectBottom.layer.opacity = 1
        },completion: { _ in
            self.imgContentEffectBottom.layer.opacity = 0
        })
    }
    
    override func loadImages() {
        loadResourceToImage("img_2d_41", imgContent)
        loadResourceToImage("img_2d_41_0", imgContent1)
        loadResourceToImage("img_2d_41_1_normal", imgContentEffectTopNorma)
        loadResourceToImage("img_2d_41_2_normal", imgContentEffectBottomNorma)
        loadResourceToImage("img_2d_41_1", imgContentEffectTop)
        loadResourceToImage("img_2d_41_2", imgContentEffectBottom)
        setupToolbar(imgBackground, imgGoldBars, imgLogo)
        
    }
}
