//
//  EndingVC.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 1/16/20.
//  Copyright © 2020 bp. All rights reserved.
//

import UIKit

class EndingVC: BaseContentVC {
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBAction func onMenuTap(_ sender: UIButton) {
        inFlowMenuTap(sender)
    }
    
    @IBAction func onEndTap(_ sender: Any) {
        presentDetail(HospitalVC(nibName: "HospitalVC", bundle: nil))
    }
    @IBAction func onBackTap(_ sender: Any) {
        dismissDetail()
    }
    
    override func loadImages() {
        loadResourceToImage("img_2d_2_7", imgBackground)
    }
    
}
