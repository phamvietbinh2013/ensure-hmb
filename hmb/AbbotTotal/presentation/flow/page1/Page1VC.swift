//
//  Page1VC.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 1/15/20.
//  Copyright © 2020 bp. All rights reserved.
//

import UIKit

class Page1VC: BaseContentVC {
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet var imgGoldBars: [UIImageView]!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet var lbContent: [UILabel]!
    
    @IBOutlet weak var imgContent: UIImageView!
    @IBAction func onMenuTap(_ sender: UIButton) {
        inFlowMenuTap(sender)
    }
    @IBAction func onNextTap(_ sender: Any) {
        presentDetail(Page2VC(nibName: "Page2VC", bundle: nil))
    }
    
    @IBAction func onBackTap(_ sender: Any) {
        dismissDetail()
    }
    
    override func loadImages() {
        loadResourceToImage("img_2d_1", imgContent)
        setupToolbar(imgBackground, imgGoldBars, imgLogo)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onViewTap)))
    }
    
    @objc func onViewTap(){
        boucingView(lbContent[0], 1.5, 0.75)
        boucingView(lbContent[1], 1.5, 0.75)
    }
    
}
