//
//  ChangeServerVC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 5/31/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class ChangeServerVC: UIViewController {

    @IBOutlet weak var txtUrl: UITextField!
    @IBOutlet weak var viewUrl: UIView!
    @IBOutlet weak var btnOk: UIButton!
    
    @IBAction func onSaveTap(_ sender: UIButton) {
        boucingView(sender, 1.1, 0.3)
        Utils.saveToCache(key: CacheKey.baseUrl, value: txtUrl.text!)
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI(){
        btnOk.applyCornerGradient(colours: [UIColor(hexString: "#9d7528")!,UIColor(hexString: "#fef25b")!,UIColor(hexString: "#9d7528")!], corner: 10)
        viewUrl.roundCorderWithBorder(UIColor(hexString: "#26a0df")!, 30)    
        txtUrl.text = Utils.getFromCache(key: CacheKey.baseUrl)
        txtUrl.placeholder = "Nhập link server mới"
        txtUrl.attributedPlaceholder = NSAttributedString(string: txtUrl.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
