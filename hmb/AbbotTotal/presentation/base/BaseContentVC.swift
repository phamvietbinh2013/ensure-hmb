//
//  BaseContentVC.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 9/20/19.
//  Copyright © 2019 bp. All rights reserved.
//

import UIKit

class BaseContentVC: UIViewController, UIPopoverPresentationControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
    func loadImages(){
        
    }
    
    func resetAnimation(){
        
    }
    
    func startAnimation(){
        
    }
    
    func onRequestEndCall(){
        showConfirmAlertWith(message: "Bạn có chắc chắn muốn kết thúc trò chuyện ?", callBackPositive: { 
            print("end")
        })
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle{
        return .none
    }
    
    func inFlowMenuTap(_ sender : UIButton){
        let viewController = InFlowMenuVC(nibName: "InFlowMenuVC", bundle: nil)
        viewController.preferredContentSize = CGSize(width: 480, height: 290)
        viewController.base = self
        let navController = UINavigationController(rootViewController: viewController)
        navController.modalPresentationStyle = .popover
        if let pctrl = navController.popoverPresentationController {
            pctrl.delegate = self
            pctrl.sourceView = sender
            pctrl.sourceRect = sender.bounds
            self.present(navController, animated: true, completion: nil)
        }
    }
    
}
