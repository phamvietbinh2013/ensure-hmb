//
//  HomeView.swift
//  SimilacNeosure
//
//  Created by Bình Phạm on 3/12/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import Foundation

protocol PageView {
    
    func onRequestEndCall()
    
    func startTimer()
    
    func pauseTimer()
    
}
