//
//  MenuOptionalVC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class HomeMenuVC: UIViewController {
    private let userData : UserData = UserData(JSONString: Utils.getFromCache(key: CacheKey.userdata)!)!
    public var base : UIViewController?
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtID: UILabel!
    @IBOutlet weak var txtVersion: UILabel!
    
    @IBAction func onChangeServerTap(_ sender: Any) {
        dismiss(animated: true, completion: {
            let vc = ChangeServerVC(nibName: "ChangeServerVC", bundle: nil)
            vc.modalPresentationStyle = .fullScreen
            self.base?.present(vc, animated: true, completion: nil)
        })
    }
    @IBAction func onLogoutTap(_ sender: Any) {
        dismiss(animated: true, completion: {
            Utils.saveToCache(key: CacheKey.userdata, value: "")
            let vc = UIStoryboard(name: "Main", bundle: nil) .
                instantiateViewController(withIdentifier: "LoginViewController")
            vc.modalPresentationStyle = .fullScreen
            self.base?.present(vc, animated: true, completion: nil)
        })
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        setupHeader()
    }
    
    func setupHeader(){
        viewHeader.applyCornerGradient(colours: [UIColor(hexString: "#1f3d76")!, UIColor(hexString: "#0279b7")!], corner: 0)
        txtName.text = userData.name?.uppercased()
        txtID.text = "ID: \(userData.sysid?.uppercased() ?? "")"
        txtVersion.text = "Phiên bản: \(getVersionString())"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
}
