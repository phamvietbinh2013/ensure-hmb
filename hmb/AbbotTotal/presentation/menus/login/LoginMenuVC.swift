//
//  MenuOptionalVC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class LoginMenuVC: UIViewController {
    
    public var base : UIViewController?
    
    @IBAction func onChangeServerTap(_ sender: Any) {
        dismiss(animated: true, completion: {
            let vc = ChangeServerVC(nibName: "ChangeServerVC", bundle: nil)
            vc.modalPresentationStyle = .fullScreen
            self.base?.present(vc, animated: true, completion: nil)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
}
