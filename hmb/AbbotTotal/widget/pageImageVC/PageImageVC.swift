//
//  PageImageVC.swift
//  Pediatric
//
//  Created by Bình Phạm on 9/4/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit

class PageImageVC: UIViewController, DetailView {
    
    @IBOutlet weak var imgBg: UIImageView!
    func loadImages() {
        loadResourceToImage(imgName!, imgBg)
    }
    
    func resetAnimation() {
        
    }
    
    func startAnimation() {
        
    }
    
    public var imgName : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let _ = imgName {
            loadImages()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
