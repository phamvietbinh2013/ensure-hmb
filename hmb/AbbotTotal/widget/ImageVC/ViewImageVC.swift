//
//  ViewImageVC.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 8/2/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class ViewImageVC: UIViewController {

    public var homeView : PageView?
    @IBOutlet weak var imgViewBG: UIImageView!
    public var image: UIImage?
    
    @IBAction func abtnOnClose(_ sender: Any) {
        dismiss(animated: true) {
            self.homeView?.startTimer()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        imgViewBG.image = image
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
