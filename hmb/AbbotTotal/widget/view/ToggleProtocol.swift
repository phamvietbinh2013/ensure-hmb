//
//  ToggleProtocol.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 9/20/19.
//  Copyright © 2019 bp. All rights reserved.
//

import Foundation
protocol ToggleProtocol {
    func showNormal()
    func showDetail()
}
