//
//  ConfirmDialogVC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/7/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class ConfirmDialogVC: UIViewController {
    
    @IBOutlet weak var btnPositive: UIButton!
    @IBOutlet weak var btnNegative: UIButton!
    @IBOutlet weak var lbMessage: UILabel!
    @IBOutlet weak var viewMessage: UIView!
    
    @IBAction func onPositiveTouch(_ sender: UIButton) {
        boucingView(sender, 1.1, 0.3)
    }
    
    @IBAction func onPositiveTap(_ sender: UIButton) {
        dismiss(animated: true, completion: {
            self.callbackPositive?(self)
        })
    }
    
    @IBAction func onNegativeTouch(_ sender: UIButton) {
        boucingView(sender, 1.1, 0.3)
    }
    
    @IBAction func onNegativeTap(_ sender: UIButton) {
        dismiss(animated: true, completion: {
            self.callbackNegative?(self)
        })
    }
    
    public var callbackPositive:((UIViewController) -> ())?
    public var callbackNegative:((UIViewController) -> ())?
    public var negativeText : String = ""
    public var positiveText : String = ""
    public var messenge : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewMessage.layer.cornerRadius = 15
        btnPositive.applyCornerGradient(colours: [UIColor(hexString: "#9d7528")!,UIColor(hexString: "#fef25b")!,UIColor(hexString: "#9d7528")!], corner: 30)
        btnNegative.applyCornerGradient(colours: [UIColor(hexString: "#9d7528")!,UIColor(hexString: "#fef25b")!,UIColor(hexString: "#9d7528")!], corner: 30)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lbMessage.text = messenge
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
