//
//  MessagerDialogVC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class MessagerDialogVC: UIViewController {
    @IBOutlet weak var lbMessenge: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var viewMessage: UIView!
    
    @IBAction func onOKTouch(_ sender: UIButton) {
        boucingView(sender, 1.1, 0.3)
    }
    
    @IBAction func onOkTap(_ sender: UIButton) {
        dismiss(animated: true, completion: {
            self.callbackPositive?(self)
        })
    }
    public var isAlert: Bool = false
    public var messenge : String = ""
    public var callbackPositive:((UIViewController) -> ())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewMessage.layer.cornerRadius = 15
        lbMessenge.text = messenge
        btnConfirm.applyCornerGradient(colours: [UIColor(hexString: "#9d7528")!,UIColor(hexString: "#fef25b")!,UIColor(hexString: "#9d7528")!], corner: 30)
    }
    
}
