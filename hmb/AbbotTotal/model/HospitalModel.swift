//
//  Hcp.swift
//
//  Created by Long Quách Phi on 5/29/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class HospitalModel: Mappable, NSCoding {

  private struct SerializationKeys {
    static let name = "name"
    static let id = "id"
  }

  public var name: String?
  public var id: String?
  public required init?(map: Map){

  }

  public func mapping(map: Map) {
    name <- map[SerializationKeys.name]
    id <- map[SerializationKeys.id]
  }

  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    return dictionary
  }

  required public init(coder aDecoder: NSCoder) {
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(id, forKey: SerializationKeys.id)
  }

}
