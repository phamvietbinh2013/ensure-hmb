//
//  HCPFilterItem.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 1/14/20.
//  Copyright © 2020 bp. All rights reserved.
//

import Foundation
import SearchTextField

class HostpitalFilterItem: SearchTextFieldItem {
    public var id: Int?
    public init(title: String, id: Int?) {
        super.init(title: title)
        self.title = title
        self.id = id
    }
}
