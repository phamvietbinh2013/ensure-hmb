//
//  BaseClass.swift
//
//  Created by Long Quách Phi on 7/3/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class ReturnDurationModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let clientid = "clientid"
    static let status = "status"
    static let msg = "msg"
  }

  // MARK: Properties
  public var clientid: String?
  public var status: String?
  public var msg: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    clientid <- map[SerializationKeys.clientid]
    status <- map[SerializationKeys.status]
    msg <- map[SerializationKeys.msg]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = clientid { dictionary[SerializationKeys.clientid] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = msg { dictionary[SerializationKeys.msg] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.clientid = aDecoder.decodeObject(forKey: SerializationKeys.clientid) as? String
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.msg = aDecoder.decodeObject(forKey: SerializationKeys.msg) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(clientid, forKey: SerializationKeys.clientid)
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(msg, forKey: SerializationKeys.msg)
  }

}
