//
//  BaseClass.swift
//
//  Created by Long Quách Phi on 7/3/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class DurationRequestModel: Mappable, NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let appid = "appid"
        static let usersysid = "usersysid"
        static let hospid = "hospid"
        static let endtime = "endtime"
        static let starttime = "starttime"
        static let appver = "appver"
        static let clientid = "clientid"
        static let longitude = "longitude"
        static let latitude = "latitude"
        static let hcpid = "hcpid"
    }
    
    // MARK: Properties
    public var appid: String?
    public var usersysid: String?
    public var hospid: String?
    public var endtime: String?
    public var starttime: String?
    public var appver: String?
    public var clientid: String?
    public var longitude : String?
    public var latitude : String?
    public var hcpid: String?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        appid <- map[SerializationKeys.appid]
        usersysid <- map[SerializationKeys.usersysid]
        hospid <- map[SerializationKeys.hospid]
        endtime <- map[SerializationKeys.endtime]
        starttime <- map[SerializationKeys.starttime]
        appver <- map[SerializationKeys.appver]
        clientid <- map[SerializationKeys.clientid]
        latitude <- map[SerializationKeys.latitude]
        longitude <- map[SerializationKeys.longitude]
        hcpid <- map[SerializationKeys.hcpid]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = appid { dictionary[SerializationKeys.appid] = value }
        if let value = usersysid { dictionary[SerializationKeys.usersysid] = value }
        if let value = hospid { dictionary[SerializationKeys.hospid] = value }
        if let value = endtime { dictionary[SerializationKeys.endtime] = value }
        if let value = starttime { dictionary[SerializationKeys.starttime] = value }
        if let value = appver { dictionary[SerializationKeys.appver] = value }
        if let value = clientid { dictionary[SerializationKeys.clientid] = value }
        if let value = latitude { dictionary[SerializationKeys.latitude] = value }
        if let value = longitude { dictionary[SerializationKeys.longitude] = value }
        if let value = hcpid {dictionary[SerializationKeys.hcpid] = value}
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.appid = aDecoder.decodeObject(forKey: SerializationKeys.appid) as? String
        self.usersysid = aDecoder.decodeObject(forKey: SerializationKeys.usersysid) as? String
        self.hospid = aDecoder.decodeObject(forKey: SerializationKeys.hospid) as? String
        self.endtime = aDecoder.decodeObject(forKey: SerializationKeys.endtime) as? String
        self.starttime = aDecoder.decodeObject(forKey: SerializationKeys.starttime) as? String
        self.appver = aDecoder.decodeObject(forKey: SerializationKeys.appver) as? String
        self.clientid = aDecoder.decodeObject(forKey: SerializationKeys.clientid) as? String
        self.latitude = aDecoder.decodeObject(forKey: SerializationKeys.latitude) as? String
        self.longitude = aDecoder.decodeObject(forKey: SerializationKeys.longitude) as? String
        self.hcpid = aDecoder.decodeObject(forKey: SerializationKeys.hcpid) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(appid, forKey: SerializationKeys.appid)
        aCoder.encode(usersysid, forKey: SerializationKeys.usersysid)
        aCoder.encode(hospid, forKey: SerializationKeys.hospid)
        aCoder.encode(endtime, forKey: SerializationKeys.endtime)
        aCoder.encode(starttime, forKey: SerializationKeys.starttime)
        aCoder.encode(appver, forKey: SerializationKeys.appver)
        aCoder.encode(clientid, forKey: SerializationKeys.clientid)
        aCoder.encode(latitude, forKey: SerializationKeys.latitude)
        aCoder.encode(longitude, forKey: SerializationKeys.longitude)
        aCoder.encode(hcpid, forKey : SerializationKeys.hcpid)
    }
    
}
