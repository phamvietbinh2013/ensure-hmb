//
//  ResponseCheckVersion.swift
//
//  Created by Long Quách Phi on 11/16/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class ResponseCheckVersion: Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let status = "status"
        static let updaterequired = "updaterequired"
        static let msg = "msg"
        static let appid = "appid"
        static let latestver = "latestver"
    }
    
    // MARK: Properties
    public var status: String?
    public var updaterequired: String?
    public var msg: String?
    public var appid: String?
    public var latestver: String?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        status <- map[SerializationKeys.status]
        updaterequired <- map[SerializationKeys.updaterequired]
        msg <- map[SerializationKeys.msg]
        appid <- map[SerializationKeys.appid]
        latestver <- map[SerializationKeys.latestver]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = updaterequired { dictionary[SerializationKeys.updaterequired] = value }
        if let value = msg { dictionary[SerializationKeys.msg] = value }
        if let value = appid { dictionary[SerializationKeys.appid] = value }
        if let value = latestver { dictionary[SerializationKeys.latestver] = value }
        return dictionary
    }
    
}
