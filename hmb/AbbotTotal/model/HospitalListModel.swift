import Foundation
import ObjectMapper

public struct HospitalListModel: Mappable {
    
    private struct SerializationKeys {
        static let hospital = "hospital"
    }
    
    public var data: [HospitalModel]?
    
    public init?(map: Map){
        
    }
    
    public mutating func mapping(map: Map) {
        data <- map[SerializationKeys.hospital]
    }
    
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = data { dictionary[SerializationKeys.hospital] = value.map { $0.dictionaryRepresentation() } }
        return dictionary
    }
    
}
