//
//  GetHCP.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import Foundation

class GetHospital {
    
    func params(_ id : String) -> GetHCPRequestModel{
        let getHCPRequest = GetHCPRequestModel(JSONString: "{}")!
        getHCPRequest.userid = id
        return getHCPRequest
    }
    
    func postHCP(id : String, callBack:@escaping (Bool, HospitalListModel) -> Void){   
        NetworkService.post(endPoint: API.downloadHospital, requestBody: params(id).toJSON()) { success,response in
            if let data = HospitalListModel(JSONString: response) {
                callBack(success, data)
            } else {
                callBack(false, HospitalListModel(JSONString: "{}")!)
            }
        }
    }
}
