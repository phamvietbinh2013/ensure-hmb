//
//  NetworkService.swift
//  ba
//
//  Created by Bình Phạm on 11/3/17.
//  Copyright © 2017 Fractal. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class NetworkService {
    
    public static func post(endPoint : String, requestBody : Parameters,
                            callBack:@escaping (Bool, String) -> Void)
    {
        var url = ""
        if let baseUrl = Utils.getFromCache(key: CacheKey.baseUrl) {
            url = baseUrl + endPoint
        }
        print("url",url)
        print("requestBody",requestBody)
        Alamofire
            .request(url,
                     method: .post,
                     parameters: requestBody,
                     encoding: JSONEncoding.default)
            .responseString(completionHandler: {response in
                if let data = response.result.value{
                    print("response",data)
                    if (!data.elementsEqual("")){
                        callBack(true,data)
                    } else {
                        callBack(false,"{}")
                    }
                }
                else{
                    callBack(false,"{}")
                }
            })
    }
    
    public static func get(endPoint : String, requestParameter : Parameters,
                           callBack:@escaping (Bool, String) -> Void)
    {
        Alamofire
            .request(endPoint,
                     method: .get,
                     parameters: requestParameter,
                     encoding: JSONEncoding.default)
            .responseString(completionHandler: {response in
                if let data = response.result.value{
                    print(data)
                    callBack(true,data)
                }
                else{
                    callBack(false,"{}")
                }
            })
    }
    
}


