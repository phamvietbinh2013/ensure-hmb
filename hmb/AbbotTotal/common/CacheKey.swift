//
//  CacheKey.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 9/18/19.
//  Copyright © 2019 bp. All rights reserved.
//

import Foundation

public class CacheKey {
    public static let userdata = "userdata"
    public static let selectedHCP = "selectedHCP"
    public static let baseUrl = "baseUrl"
    public static let DURATION_RECORD = "duration_record"
    public static let HCP_IC  = "id"
    public static let URL_IMAGECONSENT = "url_imageconsent"
    public static let HCP_SURVEYS_RECORD = "hcp_survey_record"
    public static let FLOWABLE = "flowable"
    
    //    - 0: s0
    //    - 1: similac (14) - san
    //    - 2: glucerna
    //    - 3: similac 3 flow
    //    - 4: similac 16 trang
    //    - 5: pediasure
    //    - 6: hidrasec
    //    - 7 : neosure
    //    - 8 : similac 21 trang
    
}
