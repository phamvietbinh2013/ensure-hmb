//
//  Utils.swift
//  SimilacNVE
//
//  Created by Bình Phạm on 11/20/17.
//  Copyright © 2017 Fractal. All rights reserved.
//

import Foundation
import UIKit
import AssetsLibrary

class Utils{
    
    public static let APP_ID = "210002"
    
    func convertToDictionary(text: String) -> Any? {
        
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [])
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
        
    }
    
    private static let second = 60
    
    public static func getTimeFromMili(mili : Int) -> String{
        return "\(mili/60):\(mili%60)"
    }
    
    public static func getFromCache(key : String) -> String? {
        return UserDefaults.standard.string(forKey: key)
    }
    
    public static func saveToCache(key : String, value : String){
        UserDefaults.standard.set(value, forKey: key)
    }
    
    public static func updateInstruction(page : String){
        UserDefaults.standard.set(true, forKey: page)
    }
    
    public static func isInstructed(page : String) -> Bool{
        return !UserDefaults.standard.bool(forKey: page)
    }
    
    public static func clearFromCache(key : String){
        UserDefaults.standard.removeObject(forKey: key)
    }
    
    public static func getDurationData() -> [DurationRecord]{
        let json = getFromCache(key: CacheKey.DURATION_RECORD)
        if let j = json {
            return [DurationRecord](json : j)
        } else {
            return []
        }
    }
    
    static let WEEK = 604800000
    
    public static func saveDurations(durations : [DurationRecord]) {
        var inTimeDuration : [DurationRecord] = []
        let now = Int(Date().timeIntervalSince1970*1000)
        for duration in durations {
            if (now - duration.startTick) <= WEEK{
                inTimeDuration.append(duration)
            }
        }
        saveToCache(key: CacheKey.DURATION_RECORD, value: inTimeDuration.toJsonString())
    }
    
    public static func getLastDuration() -> DurationRecord? {
        let data = getDurationData()
        if (data.count > 0){
            return data[data.count-1]
        } else {
            return nil
        }
    }
    
    public static func saveLastDuration(duration : DurationRecord) {
        var data = getDurationData()
        if (data.count>0){
            data[data.count-1] = duration
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
            let endTime = formatter.string(from: Date())
            data[data.count-1].endtime = endTime
            saveDurations(durations: data)
        }
    }
    
    public static func exportSignToBase64(_ image : UIImage) -> String{
        let imageData:NSData = image.pngData()! as NSData
        let str64 = imageData.base64EncodedString(options: .lineLength64Characters)
        return str64
    }
    
    public static func saveImage(image: UIImage,fileName: String) -> String {
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let url = documentsURL.appendingPathComponent("\(fileName).png")
        let result:NSData = image.pngData()! as NSData
        result.write(to: url, atomically: true)
        return url.path
    }
      
    public static func clearDuration(_ id : String) {
        var data = getDurationData()
        var index = -1
        if (data.count > 0){
            for i in 0...data.count-1 {
                if (data[i].clientid?.elementsEqual(id))!{
                    index = i
                    break
                }
            }
            if index>=0 {
                data.remove(at: index)
                saveDurations(durations: data)
            }
        }
    }
    
}
