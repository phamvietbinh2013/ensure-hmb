//
//  UIView+Extension.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 9/18/19.
//  Copyright © 2019 bp. All rights reserved.
//

import Foundation
import UIKit

protocol Shimmerable {
    func start(count: Int) -> Void
    func stop() -> Void
    var lightColor: UIColor {get set}
    var darkColor: UIColor {get set}
    var isShimmering: Bool {get}
}

extension UIView {
    
    func applyCornerGradient(colours: [UIColor], corner : CGFloat) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        gradient.cornerRadius = corner
        gradient.name = "gradient"
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.name = "gradient"
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func removeGradient(){
        for layer in layer.sublayers ?? []{
            if layer.name == "gradient" {
                layer.removeFromSuperlayer()
            }
        }
    }
    
    func zoomIn(_ time : Double){
        self.transform = CGAffineTransform(scaleX: 1, y: 1)
        UIView.animate(withDuration: time, delay: 0.0, options: [.curveLinear], animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        }) { (animationCompleted: Bool) -> Void in
        }
    }
    
    func setAnchorPoint(_ point: CGPoint) {
        var newPoint = CGPoint(x: bounds.size.width * point.x, y: bounds.size.height * point.y)
        var oldPoint = CGPoint(x: bounds.size.width * layer.anchorPoint.x, y: bounds.size.height * layer.anchorPoint.y);
        
        newPoint = newPoint.applying(transform)
        oldPoint = oldPoint.applying(transform)
        
        var position = layer.position
        
        position.x -= oldPoint.x
        position.x += newPoint.x
        
        position.y -= oldPoint.y
        position.y += newPoint.y
        
        layer.position = position
        layer.anchorPoint = point
    }
    
    private static let kLayerNameGradientBorder = "GradientBorderLayer"
    
    public func setGradientBorder(
        width: CGFloat,
        colors: [UIColor],
        corner : CGFloat = 0,
        startPoint: CGPoint = CGPoint(x: 0.5, y: 0),
        endPoint: CGPoint = CGPoint(x: 0.5, y: 1)
    ) {
        let existedBorder = gradientBorderLayer()
        let border = existedBorder ?? CAGradientLayer()
        border.name = UIView.kLayerNameGradientBorder
        border.frame = bounds
        border.colors = colors.map { return $0.cgColor }
        border.startPoint = startPoint
        border.endPoint = endPoint
        
        let mask = CAShapeLayer()
        mask.path = UIBezierPath(roundedRect: bounds, cornerRadius: corner).cgPath
        mask.fillColor = UIColor.clear.cgColor
        mask.strokeColor = UIColor.white.cgColor
        mask.lineWidth = width
        
        border.mask = mask
        
        let exists = existedBorder != nil
        if !exists {
            layer.addSublayer(border)
        }
    }
    
    public func removeGradientBorder() {
        self.gradientBorderLayer()?.removeFromSuperlayer()
    }
    
    private func gradientBorderLayer() -> CAGradientLayer? {
        let borderLayers = layer.sublayers?.filter { return $0.name == UIView.kLayerNameGradientBorder }
        if borderLayers?.count ?? 0 > 1 {
            fatalError()
        }
        return borderLayers?.first as? CAGradientLayer
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func roundCorderWithBorder(_ color: UIColor, _ radius : CGFloat){
        self.layer.borderWidth = 1
        self.layer.borderColor = color.cgColor
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = false
    }
    
    private struct ShimmerProperties {
        static let shimmerKey:String = "io.monroy.shimmer.key"
        static var lightColor:CGColor = UIColor.white.withAlphaComponent(0.1).cgColor
        static var darkColor:CGColor = UIColor.black.withAlphaComponent(1).cgColor
        static var isShimmering:Bool = false
        static var gradient:CAGradientLayer = CAGradientLayer()
        static var animation:CABasicAnimation = CABasicAnimation(keyPath: "locations")
    }
    var lightColor: UIColor {
        get {
            return UIColor(cgColor: ShimmerProperties.lightColor)
        }
        set {
            ShimmerProperties.lightColor = newValue.cgColor
        }
    }
    var darkColor: UIColor {
        get {
            return UIColor(cgColor: ShimmerProperties.darkColor)
        }
        set {
            ShimmerProperties.darkColor = newValue.cgColor
        }
    }
    
    var isShimmering: Bool {
        get {
            return ShimmerProperties.isShimmering
        }
    }
    
    func stopShimmer() {
        guard ShimmerProperties.isShimmering else {return}
        self.layer.mask?.removeAnimation(forKey: ShimmerProperties.shimmerKey)
        self.layer.mask = nil
        ShimmerProperties.isShimmering = false
        self.layer.setNeedsDisplay()
    }
    
    func startShimmer(count: Int = 0) {
        guard !ShimmerProperties.isShimmering else {return}
        
        CATransaction.begin()
        CATransaction.setCompletionBlock({
            self.stopShimmer()
        })
        
        ShimmerProperties.isShimmering = true
        
        ShimmerProperties.gradient.colors = [ShimmerProperties.lightColor, ShimmerProperties.darkColor, ShimmerProperties.lightColor];
        ShimmerProperties.gradient.frame = CGRect(x: CGFloat(-2*self.bounds.size.width), y: CGFloat(0.0), width: CGFloat(4*self.bounds.size.width), height: CGFloat(self.bounds.size.height))
        ShimmerProperties.gradient.startPoint = CGPoint(x: Double(0), y: Double(0.5));
        ShimmerProperties.gradient.endPoint = CGPoint(x: Double(1), y: Double(0.625));
        ShimmerProperties.gradient.locations = [0.45, 0.5, 0.55];
        
        ShimmerProperties.animation.duration = 1
        ShimmerProperties.animation.repeatCount = (count > 0) ? Float(count) : .infinity
        ShimmerProperties.animation.fromValue = [0.0, 0.1, 0.2]
        ShimmerProperties.animation.toValue = [0.8, 0.9, 1.0]
        
        ShimmerProperties.gradient.add(ShimmerProperties.animation, forKey: ShimmerProperties.shimmerKey)
        self.layer.mask = ShimmerProperties.gradient;
        
        CATransaction.commit()
    }
    
}
