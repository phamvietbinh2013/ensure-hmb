//
//  UIController+Extension.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 9/18/19.
//  Copyright © 2019 bp. All rights reserved.
//

import Foundation
import UIKit
import PopupDialog
import Nuke

extension UIViewController {
    
    func setupToolbar(_ background : UIImageView, _ goldBars : [UIImageView], _ logo : UIImageView){
        loadResourceToImage("bg_chart", background)
        loadResourceToImage("logo", logo)
        for bar in goldBars {
            loadResourceToImage("img_gold_bar", bar)
        }
    }
    
    func setupToolbarUnisversal(_ background : UIImageView, _ goldBars : [UIImageView], _ logo : UIImageView){
        loadResourceToImage("bg_universal", background)
        loadResourceToImage("logo", logo)
        for bar in goldBars {
            loadResourceToImage("img_gold_bar", bar)
        }
    }
    
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        viewControllerToPresent.modalPresentationStyle = .fullScreen
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window!.layer.add(transition, forKey: kCATransition)
        present(viewControllerToPresent, animated: false)
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window!.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false)
    }
    
    func showAlertWith(message: String, callBackPositive: @escaping (()->()) = {}){
        let vc = MessagerDialogVC(nibName: "MessagerDialogVC", bundle: nil)
        vc.messenge = message
        vc.isAlert = true
        vc.callbackPositive = { (_) in
            callBackPositive()
        }
        let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceDown, preferredWidth: 500, tapGestureDismissal: true, panGestureDismissal: false, hideStatusBar: false, completion: nil)
        popup.view.backgroundColor = UIColor.clear
        let overlayAppearance = PopupDialogOverlayView.appearance()
        overlayAppearance.blurRadius = 50
        overlayAppearance.blurEnabled = true
        overlayAppearance.liveBlurEnabled = true
        present(popup, animated: true, completion: nil)
    }
    
    func showConfirmAlertWith(message: String, callBackPositive: @escaping (()->()) = {}, callBackNegative: @escaping (()->()) = {}){
        let vc = ConfirmDialogVC(nibName: "ConfirmDialogVC", bundle: nil)
        vc.messenge = message
        vc.callbackPositive = { (_) in
            callBackPositive()
        }
        vc.callbackNegative = { (_) in
            callBackNegative()
        }
        let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceDown, preferredWidth: 500, tapGestureDismissal: true, panGestureDismissal: false, hideStatusBar: false, completion: nil)
        popup.view.backgroundColor = UIColor.clear
        let overlayAppearance = PopupDialogOverlayView.appearance()
        overlayAppearance.blurRadius = 50
        overlayAppearance.blurEnabled = true
        overlayAppearance.liveBlurEnabled = true
        present(popup, animated: true, completion: nil)
    }
    
    func getVersionString() -> String{
        return "\(Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String)"
    }
    
    func zoomIn(_ content : UIView, _ time : Double){
        content.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        UIView.animate(withDuration: time, delay: 0.0, options: [.curveLinear], animations: { () -> Void in
            content.transform = CGAffineTransform.identity
        }) { (animationCompleted: Bool) -> Void in
        }
    }
    
    func loadResourceToImage(_ resourceName : String, _ image : UIImageView){
        let path = Bundle.main.url(forResource: resourceName, withExtension: "png")
        Nuke.loadImage(with: path!, into: image)
    }
    
    func showHightlight(content : UIView, color : UIColor, radius : CGFloat){
        content.layer.shadowColor = color.cgColor
        content.layer.shadowOpacity = 1
        content.layer.shadowOffset = CGSize.zero
        content.layer.shadowRadius = radius
        content.layer.shouldRasterize = true
    }
    
    func twinkleImageView(_ content : UIView){
        let viewTwinkle = UIView()
        viewTwinkle.frame = CGRect(x: content.frame.origin.x, y: content.frame.origin.y, width: content.bounds.size.width, height: content.bounds.size.height)
        view.addSubview(viewTwinkle)
    }
    
    func rotateView(_ content : UIView, _ time : Double){
        var rotationAnimation = CABasicAnimation()
        rotationAnimation = CABasicAnimation.init(keyPath: "transform.rotation.z")
        rotationAnimation.toValue = NSNumber(value: (Double.pi * 2.0))
        rotationAnimation.duration = time
        rotationAnimation.isCumulative = true
        rotationAnimation.repeatCount = .infinity
        content.layer.add(rotationAnimation, forKey: "rotationAnimation")
    }
    
    func boucingView(_ content : UIView, _ scale : CGFloat, _ time : Double){
        content.transform = CGAffineTransform(scaleX: scale, y: scale)
        UIView.animate(withDuration: time, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 6.0, options: [], animations: {
            content.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    func boucingViewForever(_ content : UIView, _ scale : CGFloat){
        content.transform = CGAffineTransform(scaleX: scale, y: scale)
        UIView.animate(withDuration: 1.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.5, options: [.repeat, .autoreverse, .allowUserInteraction,], animations: {
            content.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
}
