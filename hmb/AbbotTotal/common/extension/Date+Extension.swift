//
//  Date+Extension.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 9/18/19.
//  Copyright © 2019 bp. All rights reserved.
//

import Foundation
extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}

