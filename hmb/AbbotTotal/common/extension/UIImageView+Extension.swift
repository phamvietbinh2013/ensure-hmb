//
//  UIImageView+Extension.swift
//  AbbotTotal
//
//  Created by Bình Phạm on 1/15/20.
//  Copyright © 2020 bp. All rights reserved.
//

import Foundation
import UIKit
extension UIImageView {
    func tintImageColor(color : UIColor) {
        self.image = self.image!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.tintColor = color
    }
}
